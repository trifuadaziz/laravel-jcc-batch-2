<!-- gunakan layout master -->
@extends('layout.master')  

@section('judul')
Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="post">      <!-- method post untuk ngirim data -->
        @csrf  <!-- token supaya tidak ada tabrakan data-->

        <label>First Name :</label> <br> <br>
        <input type="text" name="Nama Awal" required> <br> <br>
        <label>Last Name :</label> <br> <br>
        <input type="text" name="Nama Akhir" required> <br> <br>

        <label>Gender</label> <br> <br>
        <input type="radio" name="jenis kelamin" value="1" required> Male <br>
        <input type="radio" name="jenis kelamin" value="2" required> Female <br> <br>

        <label>Nationality</label> <br> <br>
        <select name="bahasa" required>
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>            
        </select> <br> <br>

        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" name="language"> English <br>
        <input type="checkbox" name="language"> Other <br> <br>


        <label>Bio</label> <br> <br>
        <textarea name="biodata" cols="30" rows="10" required></textarea> <br>

        <input type="submit" value="Sign Up">
    </form>
@endsection