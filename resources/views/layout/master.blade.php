<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Blank Page</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <!-- asset itu adalah masuk ke folder public -->
  <link rel="stylesheet" href="{{asset("layout/plugins/fontawesome-free/css/all.min.css")}}">
  <!-- Theme style -->
  <!-- asset itu adalah masuk ke folder public -->
  <link rel="stylesheet" href="{{asset("layout/dist/css/adminlte.min.css")}}">
  
  <!-- stack "style" dimasukkan ke master blade -->
  @stack('style')
  
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  
  <!-- Navbar -->

  <!-- masukkan data yg ada di nav.blade.php -->
  @include('partial.nav')

  <!-- /.navbar -->
  
  <!-- Main Sidebar Container -->

  <!-- masukkan data yg ada di sidebar.blade.php -->
  @include('partial.sidebar')

  <!-- /.sidebar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('judul')</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">@yield('judul')</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          @yield('content')
        </div>
        <!-- /.card-body -->
        
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.2.0
    </div>
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- asset itu adalah masuk ke folder public -->
<script src="{{asset("layout/plugins/jquery/jquery.min.js")}}"></script>
<!-- Bootstrap 4 -->
<!-- asset itu adalah masuk ke folder public -->
<script src="{{asset("layout/plugins/bootstrap/js/bootstrap.bundle.min.js")}}"></script>
<!-- AdminLTE App -->
<!-- asset itu adalah masuk ke folder public -->
<script src="{{asset("layout/dist/js/adminlte.min.js")}}"></script>
<!-- AdminLTE for demo purposes -->
<!-- asset itu adalah masuk ke folder public -->
<script src="{{asset("layout/dist/js/demo.js")}}"></script>

<!-- stack "scripts" dimasukkan ke master blade -->
@stack('scripts')

</body>
</html>
