<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard(){        // fungsi dashboard yg mereturn ke bagian views -> home.blade.php
        return view('home');        
    }
}
