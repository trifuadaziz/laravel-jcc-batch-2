<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendaftaran(){           // fungsi pendaftaran yg mereturn ke bagian views -> halaman -> pendataan.blade.php
        return view('halaman.pendataan');
    }

    public function selamatdatang(Request $request){
        // dd($request -> all());  // akan menampilkan semua request inputan html

        $namaawal = $request['Nama_Awal'];    // input name di html pendataan.blade.php
        $namaakhir = $request['Nama_Akhir'];    // input name di html pendataan.blade.php
        
        return view('halaman.welcome', compact('namaawal', 'namaakhir'));
    
    }
}
