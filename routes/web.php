<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route Home
Route::get('/', 'HomeController@dashboard');           // buat route utk menuju ke controller 'HomeController' dengan fungsi 'dashboard' di controllernya
                                                       // '/' -> saat pertama kali halaman dijalankan (default route)
// Route Register
Route::get('/register', 'AuthController@pendaftaran'); // buat route utk menuju ke controller 'AuthController' dengan fungsi 'pendaftaran' di controllernya

// Route Welcome
Route::post('/welcome', 'AuthController@selamatdatang'); // buat route utk menuju ke controller 'AuthController' dengan fungsi 'selamatdatang' di controllernya

// method post untuk ngirim data ; method get untuk ngambil data

// Route table
Route::get('/table', function(){
    return view('halaman.table');
});

// Route data-tables
Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

// Route untuk mengarah ke form create data untuk table cast
Route::get('/cast/create', 'CastController@create');

// Route untuk menyimpan data ke table cast
Route::post('/cast', 'CastController@store');

// Route untuk menampilkan list data table cast
Route::get('/cast', 'CastController@index');

// Route untuk detail data cast dengan id tertentu
Route::get('/cast/{cast_id}', 'CastController@show');

// Route untuk mengarah ke form edit data (update)
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

// Route untuk update data ke table cast
Route::put('/cast/{cast_id}', 'CastController@update');

// Route  untuk delete data dengan id tertentu
Route::delete('/cast/{cast_id}', 'CastController@destroy');